const { Context, register } = require('../dist');

const start = async () => {
  const context = new Context({
    accessToken: 'SYSTEMUSER',
    requestId: '123'
  });
  await context.initialize();


  // context.user and context.log are now populated
}

const start2 = async () => {
  const context = new Context({
    accessToken: 'SYSTEMUSER',
    requestId: '123'
  });

  register(async (ctx) => {
    ctx.myStuff = await getSomeData()
  });

  await context.initialize();
}