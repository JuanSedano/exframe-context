'use strict';

const Sequelize = require('sequelize');

const Drivers = require('./lib/drivers');
const SequelizeDriver = require('./lib/drivers/sequelize');
const SqlConnector = require('./lib/sqlConnector');

/**
 * This method assumes you want a Sequelize driver instantiated
 *
 * @param {Sequelize.Options & {name: string, logger: object}} options
 * @returns {Promise<SqlConnector>}
 */
const init = options => {
  const connector = new SqlConnector(SequelizeDriver, options);

  return connector.connect();
};

module.exports = {
  init,
  SqlConnector,
  Drivers,
  Sequelize
};
