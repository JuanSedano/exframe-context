import { RegisterOptions } from './types/RegisterOptions';
import { MiddlewareCallback } from './types/MiddlewareCallback';

const MiddlewareProvider = {
  methods: new Map<string, RegisterOptions>(),
  entries() {
    return Array
      .from(this.methods.values())
      .sort((a, b) => (a.priority || 9999) - (b.priority || 9999))
  },
  /**
   * Returns true if the method was succesfully registered or false if it wasn't
   * Method names must be unique in order to be registered
   * */
  register(arg: RegisterOptions | MiddlewareCallback): boolean {
    let options: RegisterOptions;
    if (typeof arg === 'function') {
      options = {
        name: arg.name,
        priority: 9999,
        callback: arg,
        force: false
      };
    } else {
      options = arg;
    }

    const {
      name, force
    } = options;

    if (this.methods.has(name) && force === false) {
      return false;
    }

    this.methods.set(name, options);

    return true;
  }
};

export default MiddlewareProvider;
