import { Context } from './context';
import MiddlewareProvider from './middlewareProvider';

export const createContext = (data) => new Context(data);
export const register = MiddlewareProvider.register;
export default Context;
