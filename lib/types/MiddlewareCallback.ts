import { IContext } from './Context';

export type MiddlewareCallback = (context: Partial<IContext>) => Promise<Object | void>;
