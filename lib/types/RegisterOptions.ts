import { MiddlewareCallback } from './MiddlewareCallback';

export interface RegisterOptions {
  callback: MiddlewareCallback;
  name: string;
  priority?: number;
  force?: boolean;
}
