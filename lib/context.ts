import MiddlewareProvider from './middlewareProvider';

interface ContextOptions {
  options?: object;
  [key: string]: any;
}

export class Context {
  public log;

  #initialized: boolean;
  #initializing?: Promise<void>;

  constructor({ options, ...data }: ContextOptions) {
    Object.assign(this, data.options);

    this.#initialized = false;
    this.initialize.bind(this);

    // default log
    this.log = console.log;
  }

  async initialize() {
    let resolve;

    if (this.#initialized) {
      return;
    }

    if (this.#initializing) {
      return await this.#initializing;
    }

    this.#initializing = new Promise((r) => {
      resolve = r;
    });

    for (const { name, callback } of MiddlewareProvider.entries()) {
      try {
        const results = await callback(this);

        if (results) {
          Object.assign(this, results);
        }
      } catch (e) {
        if (this.log) {
          this.log.warn(`[${name}] Failed`, e);
        }

        throw e;
      }
    }

    resolve();

    this.#initialized = true;
  }
}
